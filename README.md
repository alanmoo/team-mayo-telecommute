# High-Level Project Description
Build a system to help manage the telecommuting schedule in WebComm, and maybe replace OOO too.

# User Roles
The following user roles should be implemented:

- Staff member
- Manager
- Administrator

These roles are hierarchical - a manager is also a staff member; an administrator will also be a manager and a staff member.

# User Stories
As a **staff member**, I should be able to:

- ...see the days that I'm scheduled in and out of the office.
- ...request a "swap" to change the schedule on a selected day, which must be approved by my team's manager.
- ...indicate that I am taking leave for a day or range of days on a presubmitted basis.
- ...indicate my location for the current day (1300/575/remote/offsite/sick/PT/etc.)
- ...quickly/easily see where the other members of my team or the department are on a given day.

As a **manager**, I should be able to:

- ...quickly/easily see the planned schedule for my staff over the coming days.
- ...change the schedule for my staff members, either on an instance basis or a recurring one.
- ...approve "swaps" and "day trades" for my staff members.

As an **administrator**, I should be able to:

- ...create/edit/delete "units" that contain one manager and one or more staff members.  (Think of these as ITS divisions.)
- ...add/edit/delete users from the system.
- ...edit the schedule for any staff member.
- ...the ability to add WCMC holidays so that days don't appear on people's schedules.