'use strict';


// Declare app level module which depends on filters, and services
angular.module('telecommuteApp', ['telecommuteApp.filters', 'telecommuteApp.services', 'telecommuteApp.directives', 'telecommuteApp.controllers','ngRoute', 'ui.bootstrap','ui.select2']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/:year/:month',{templateUrl: 'partials/month.html',
      controller: 'monthCtrl',
      resolve:{
        'getUserListData':function(fetchData){
          return fetchData.userListPromise;
        }
      }
    });
    $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller:'loginCtrl'});
    $routeProvider.when('/settings', {templateUrl: 'partials/settings.html', controller:'settingsCtrl'});
    $routeProvider.when('/admin', {templateUrl: 'partials/admin.html', controller:'adminCtrl'});
    $routeProvider.when('/:year/:month/week:week', {templateUrl: 'partials/week.html',
      controller:'weekCtrl',
      resolve:{
        'getUserListData':function(fetchData){
          return fetchData.userListPromise;
        }
      }
    });
    $routeProvider.otherwise({redirectTo: function(){
            var today= new Date();
            return "/"+today.getFullYear()+"/"+today.getMonth();
        }
        // ,
        // templateUrl: 'partials/month.html', controller: 'monthCtrl'
      });
  }]).
  run(function($rootScope, $location){
  	$rootScope.$on("$routeChangeStart", function(event, next, current){
  		if ($rootScope.currentUser == null){
  			$location.path("/login");
  		}
  	})
  }).
  //Prevent memory leaks when opening and closing daily view dialogs on month view
  run(['$dialog',function($dialog){
    var dialog = $dialog.dialog(),
    fn = dialog.__proto__._onCloseComplete;
    dialog.__proto__._onCloseComplete = function () {
    fn.apply(this);
      if (this.options.controller) {
          this.$scope.$destroy();
      }
    }
  }]
);
