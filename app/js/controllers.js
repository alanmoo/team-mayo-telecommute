'use strict';

/* Controllers */

angular.module('telecommuteApp.controllers', []).
config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]).
controller('loginCtrl', ['$rootScope', '$scope',
    function($rootScope, $scope) {
        $scope.login= function(user){
            $rootScope.currentUser = user;
        }
        $scope.manager = function() {
            $rootScope.userLevel = 'manager';
        }
        $scope.admin = function(){
            $rootScope.userLevel = 'admin';
        }
    }
]).
controller('rootCtrl', ['$rootScope', '$http', '$routeParams','dataManipulation', 'calendarGenerator', 'fetchData',
    function($rootScope, $http, $routeParams, dataManipulation, calendarGenerator, fetchData) {
        $rootScope.MONTH = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $rootScope.today = new Date();
        $rootScope.currentUser = null;
        $rootScope.logOut = function(){
            $rootScope.currentUser = null;
        }
        $http.get('http://140.251.8.194/teamMayo/getStatusList.cfm').
            success(function(data){
                $rootScope.statusList = data;
            }).
            error(function(){
                window.console.log("unable to get statuses");
            }).then(function(){
                $rootScope.locations = Object.keys($rootScope.statusList);
            });

        fetchData.userListPromise.then(function(){
        $rootScope.nameMap = fetchData.getUserList();
        $rootScope.allUsers = Object.keys($rootScope.nameMap);
        });
        $rootScope.selectedUsers = [];
        $rootScope.stopProp = function(event){
            event.stopPropagation();
        }

        fetchData.groupsListPromise.then(function(){
            $rootScope.groups = fetchData.getGroupsList();
        });
        $rootScope.selectGroup = function(groupName, $event){
            if(!$event.originalEvent.shiftKey){
                $rootScope.selectedUsers=[];
            }
            for(var user in $rootScope.groups[groupName]){
                if($rootScope.selectedUsers.indexOf($rootScope.groups[groupName][user]) == -1){
                    $rootScope.selectedUsers.push($rootScope.groups[groupName][user]);
                }
            }
        }
        $rootScope.isToday = function(dayObj){
          return calendarGenerator.isToday(dayObj);
        }

        fetchData.statusIDPromise.then(function(){
            $rootScope.statusID= fetchData.getStatusID();
        })
        // $scope.selectedUsers = function(){
        //         if (!$rootScope.selectedUsers.length){
        //             return $scope.allUsers;
        //         } else return $rootScope.selectedUsers;
        //     }
    }
]).
controller('monthCtrl', ['$rootScope', '$scope', '$routeParams', 'calendarGenerator', 'dataManipulation', '$http', '$window', '$dialog', '$filter', 'fetchData',
    function($rootScope, $scope, $routeParams, calendarGenerator, dataManipulation, $http, $window, $dialog, $filter, fetchData) {
    //     $http.get('userdata.json').
    //     success(function(data) {
    //         $scope.userData = data;
    //     });
        $scope.month = $routeParams.month;
        $scope.year = $routeParams.year;
        $scope.nextMonth = calendarGenerator.getNextMonth($scope.month);
        $scope.prevMonth = calendarGenerator.getPrevMonth($scope.month);
        $scope.nextMonthYear = calendarGenerator.getYearOfNextMonth($scope.month, $scope.year);
        $scope.prevMonthYear = calendarGenerator.getYearOfPrevMonth($scope.month, $scope.year);
        $scope.firstSunday = calendarGenerator.getFirstSunday($scope.month, $scope.year);
        $scope.lastSaturday = calendarGenerator.getLastSaturday(new Date($scope.year, $scope.month, 1), $scope.firstSunday);
        // $http.get('http://140.251.8.194/teamMayo/getCalData2.cfm?fromDate=' + calendarGenerator.queryFormatDate($scope.firstSunday) + '&todate=' + calendarGenerator.queryFormatDate($scope.lastSaturday)).
        // $http.get('fakedata.json').
        $http.get('http://140.251.8.194/teamMayo/getCalData2.cfm?fromDate=' + calendarGenerator.queryFormatDate($scope.firstSunday) + '&toDate=' + calendarGenerator.queryFormatDate($scope.lastSaturday)).
        success(function(data) {
            $scope.calendarMonth = data;
        }).
        error(function() {
            $window.console.log("No data retreived");
        });
        //http://140.251.8.194/teamMayo/getCalDatetest.cfm?fromDate=01-jun-2013&todate=31-dec-2013
        // $http.get('http://140.251.8.194/teamMayo/getCalData.cfm').
        $scope.nameMap = fetchData.getUserList();
        $scope.allUsers = Object.keys($scope.nameMap);
        $scope.selectedUsers = function(){
                if (!$rootScope.selectedUsers.length){
                    return $scope.allUsers;
                } else return $rootScope.selectedUsers;
            }
        // $scope.setLocation = function(userID, statusname, dayObj){
        //     if(!dayObj.people.hasOwnProperty(userID)){
        //         dayObj.people[userID]={};
        //     }
        //     dayObj.people[userID]["location"] = statusname;
        // }
        $scope.openDayDetails = function(date, event, fetchData){
          var d = $dialog.dialog({
                backdrop: true,
                backdropClick: true,
                resolve: {
                    //item: function(){ return angular.copy(item); },
                    dayObj: function() {
                        return $scope.calendarMonth[date];
                    },
                    offset: function() {
                        return {"xOffset":event.pageX, "yOffset":event.pageY};
                    },
                    selectedUsers: function(){
                        return $scope.selectedUsers();
                    },
                    getUserListData:function(fetchData){
                          return fetchData.userListPromise;
                    }
                }    
            });
            d.open('partials/dialogs/daily-locations.html', 'dailyLocationsCtrl');
        }
        $scope.clearFilter= function(){
            $rootScope.selectedUsers.length= 0;
        }
        $scope.selectedUsers = function(){
            if (!$rootScope.selectedUsers.length){
                return $scope.allUsers;
            } else return $rootScope.selectedUsers;
        };
        $scope.getPerson = function(userID, dayObj){
          return dayObj.people[userID];
        }
        $scope.isWeekend = function(dayObj){
            if(dayObj.dayOfWeek === "Saturday" || dayObj.dayOfWeek === "Sunday"){
                return true;
            }
            return false;
        }
    }
]).
controller('weekCtrl', ['$rootScope', '$scope', '$routeParams', 'calendarGenerator', 'dataManipulation', '$http', '$window', '$dialog', '$filter','fetchData',
    function($rootScope, $scope, $routeParams, calendarGenerator, dataManipulation, $http, $window, $dialog, $filter,fetchData) {
    //     $http.get('userdata.json').
    //     success(function(data) {
    //         $scope.userData = data;
    //     });
        $scope.month = $routeParams.month;
        $scope.year = $routeParams.year;
        $scope.weekNum = parseInt($routeParams.week, 10);
        $scope.week = calendarGenerator.getWeek($scope.year, $scope.month, $scope.weekNum);
        $scope.prevMonth = calendarGenerator.getPrevMonth($scope.month);
        $scope.nextMonthYear = calendarGenerator.getYearOfNextMonth($scope.month, $scope.year);
        $scope.prevMonthYear = calendarGenerator.getYearOfPrevMonth($scope.month, $scope.year);
        $scope.firstSunday = calendarGenerator.getFirstSunday($scope.month, $scope.year);
        $scope.lastSaturday = calendarGenerator.getLastSaturday(new Date($scope.year, $scope.month, 1), $scope.firstSunday);
        // $http.get('http://140.251.8.194/teamMayo/getCalData2.cfm?fromDate=' + calendarGenerator.queryFormatDate($scope.week.firstDateOfWeek) + '&toDate=' + calendarGenerator.queryFormatDate($scope.week.lastDateOfWeek)).
        $http.get('http://140.251.8.194/teamMayo/getCalData2.cfm?fromDate=' + calendarGenerator.queryFormatDate($scope.week.firstDateOfWeek) + '&toDate=' + calendarGenerator.queryFormatDate($scope.week.lastDateOfWeek)).
        success(function(data) {
            $scope.calendarWeek = data;
        }).
        error(function() {
            $window.console.log("No data retreived");
        });
        //http://140.251.8.194/teamMayo/getCalDatetest.cfm?fromDate=01-jun-2013&todate=31-dec-2013
        // $http.get('http://140.251.8.194/teamMayo/getCalData.cfm').

        // fetchData.getUserList().then(function(data){
        //     $scope.nameMap = data;
        //     $scope.allUsers = Object.keys(data);
        //     $scope.selectedUsers = function(){
        //         if (!$rootScope.selectedUsers.length){
        //             return $scope.allUsers;
        //         } else return $rootScope.selectedUsers;
        //     }
        // }); 
        $scope.nameMap = fetchData.getUserList();
        $scope.allUsers = Object.keys($scope.nameMap);
        $scope.selectedUsers = function(){
                if (!$rootScope.selectedUsers.length){
                    return $scope.allUsers;
                } else return $rootScope.selectedUsers;
            }
        // $scope.setLocation = function(userID, statusname, dayObj){
        //     // var userIDIndex = dayObj.people.map(function(el){return el.userID;}).indexOf(userID);
        //     dayObj.people[userID].location = statusname;
        // }
        

        $scope.clearFilter= function(){
            $rootScope.selectedUsers.length= 0;
        }
        // $scope.$watch("search", function(query){
        //   $scope.filteredData = $filter($scope.calendarMonth,query);
        // });
        $scope.doesUserManage = function(userID){
            return dataManipulation.doesUserManage($rootScope.currentUser, $scope.nameMap, userID);
        }
        $scope.getPerson = function(userID, day){
          return day.people[userID];
        }
        $scope.weekdays = function(){
            var weekdays ={}
            angular.forEach($scope.calendarWeek, function(value, key){
                if(value.dayOfWeek === "Saturday" || value.dayOfWeek==="Sunday"){
                    return;
                } else{
                    weekdays[key]=value;
                }
            });
            return weekdays;
        }
    }
]).
controller('dailyLocationsCtrl', ['$rootScope', '$scope', 'dialog', 'dataManipulation','fetchData', 'dayObj', 'offset', 'selectedUsers',
    function($rootScope, $scope, dialog, dataManipulation, fetchData, dayObj, offset, selectedUsers) {
        //Place modal based on location of cursor
        if(offset.xOffset > window.innerWidth*.75){
            //TODO: figure out a way to not have a magic number here.
            //angular.element(dialog.modalEl)[0].offsetWidth returns 0
            offset.xOffset -= 100;
        }
        $scope.selectedUsers = selectedUsers;
        angular.element(dialog.modalEl[0]).css({
            top: offset.yOffset + "px",
            left: offset.xOffset + "px"
        });
        $scope.dayObj = dayObj;

        $scope.nameMap = fetchData.getUserList();
        $scope.allUsers = Object.keys($scope.nameMap);

        $scope.doesUserManage = function(userID){
            return dataManipulation.doesUserManage($rootScope.currentUser, $scope.nameMap, userID);
        }
        $scope.getPerson = function(userID){
          return $scope.dayObj.people[userID];
        }
        // $scope.setLocation = function(userID, location){
        //     $scope.dayObj.people[userID].location = location;
        // }

        //Can be used to add a close button. Probably should be used for mobile
        $scope.close = function() {
            dialog.close('ok');
        };
    }
]).
controller('weekDayLocCtrl',['$scope', '$filter', function($scope, $filter){
    //TODO: refactor this somehow to reduce calls? 
    $scope.getUsersAtLocation= function(){
        var locations = ($filter('filterUsers')($scope.dayObj.people, $scope.selectedUsers()));
        var locationCounts = ($filter('locationFilter')(locations, $scope.loc));
        return locationCounts;
    }
    //Reduces looping somewhat
    $scope.$watchCollection('getUsersAtLocation()', function() {
        $scope.usersAtLocation = $scope.getUsersAtLocation();
    });
    //Exists to enable ng-repeat to work by passing it a number
    $scope.range = function(n) {
            if(n<0){return [];}
            var arr = new Array(n);
            for(var x=0; x<arr.length;x++){
                arr[x]=x;
            }
            return arr;
        };
}]).
controller('changeLocationCtrl',['$scope', 'dataManipulation', '$filter', 'fetchData', function($scope, dataManipulation, $filter, fetchData){
    //Should this be a service accessible to the month controller?
    $scope.seatsRemaining = function(location, dayObj){
        var capacity = $scope.statusList[location].capacity;
        if(capacity == 0){return;}
        var seatsTaken = ($filter('locationFilter')(dayObj.people, location));
        return capacity-seatsTaken;
    }
    fetchData.statusListPromise.then(function(){
        $scope.statusList = fetchData.getStatusList();
      });
    $scope.setLocation = dataManipulation.setLocation;
}]).
controller('adminCtrl', ['$scope', '$http',
    function($scope, $http) {
        $scope.clearForm = function(form){
            $scope.newUser = {};
            form.$setPristine();
            window.console.log("Form Cleared");
        }
        $scope.addUser = function(newUser){
            $http.get('http://140.251.8.194/teamMayo/userAdmin.cfm?data='+newUser.cwid+','+newUser.firstName+','+newUser.lastName+','+newUser.department+','+newUser.cwid+'@med.cornell.edu,'+newUser.department+','+newUser.role)
                .success(function(data){
                    $scope.clearForm($scope.addUserForm);
                    window.console.log("User Submitted");
                })
                .error(function(){
                    window.console.log("Error adding user");
                });
        }
    }
]).
controller('settingsCtrl',['$scope','$rootScope','$http',
    function($scope, $rootScope, $http){
        $scope.weekdays=["Monday","Tuesday","Wednesday","Thursday","Friday"];
    }]);
