/*global angular: true */
// 'use strict';

/* Directives */


angular.module('telecommuteApp.directives', [])
  //Enables an attriute called "stopProp" to get an element to halt event bubbling.
  //Doesn't work
  .directive('stopProp', function () {
    return {
      restrict: 'A',
      link: function (scope, element) {
        element.bind(function (e) {
          e.stopPropagation();
        });
      }
    };
  })
  .directive('focusActive', function(){
    return {
      restrict: "A",
      // The linking function will add behavior to the template
      link: function(scope, element, attrs) {
            attrs.$observe('checked', function(){
              if (element[0].checked){
                element.focus();
            }
            });
      }
    };
  })
  .directive('locationChanger', function factory(){
    var directiveDefinitionObject={
      restrict: "E",
      scope:{
        userID: '=user',
        dayObj: '=day'
      },
      replace: true,
      templateUrl: 'partials/directives/changeLocation.html',
      link: function(scope, element, attrs) {
      }
    };
    return directiveDefinitionObject;
  })
  .directive('accessLevel', ['$rootScope', function($rootScope, Auth) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var prevDisp = element.css('display');
            $rootScope.$watch('userLevel', function(role) {
                if($rootScope.nameMap[$rootScope.currentUser].userrole !== attrs.accessLevel){
                    element.css('display', 'none');
                  }
                else{
                    element.css('display', prevDisp);
                  }
            });
        }
    }
  }])
  .directive('loading', function(){
    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      // scope: {}, // {} = isolate, true = child, false/undefined = no change
      // cont­rol­ler: function($scope, $element, $attrs, $transclue) {},
      // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      restrict: 'E',
      template: '<div class="ui-spinner"><span class="side side-left"><span class="fill"></span></span><span class="side side-right"><span class="fill"></span></span></div>',
      // templateUrl: '',
      replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function($scope, iElm, iAttrs, controller) {
       window.console.log(); 
      }
    };
  });;