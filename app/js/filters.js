'use strict';

/* Filters */

angular.module('telecommuteApp.filters', []).
  //From input of dayObj.people, outputs only persons who are in 'selectedNames' array
  //If no selected names, returns original dayObj.people
  filter('filterUsers', function(){
    return function(input, selectedNames) {
      var returnObj = [],
          inputKeys = Object.keys(input);
      if(typeof selectedNames === 'undefined' || selectedNames.length === 0){ return input;}
      for(var x=0; x<inputKeys.length; x++){
        for(var y=0; y<selectedNames.length; y++){
          if(inputKeys[x] === selectedNames[y]){
            returnObj.push(input[inputKeys[x]]);
          }
        }
      }
      return returnObj;
    }
  }).
  //From input of people object, count number of employees at passed in "loc"
  filter('locationFilter', function(){
  	return function(input, loc){
  		var count=0;
      if (Array.isArray(input)){ 
    		input.forEach(function(el){
    			if (el.location === loc){
    				count++;
    			}
    		});
      } else {
        for(var person in input){
          if (input[person].location === loc){
            count++;
          }
        }
      }
  		return count;
  	}
  }).
  filter('seatsTaken',['$filter',function(filter){
    return function(locations, peopleObj, selectedUsers){
        var workingArray = [];
        //Get people from peopleObj that are in selectedUsers
        var filteredUsers = (filter('filterUsers')(peopleObj, selectedUsers));
        //Find out how many people are at each location
        for (var loc in locations){
          var tempObj = new Object();
          tempObj.name = locations[loc]
          tempObj.seatsTaken = (filter('locationFilter')(filteredUsers,locations[loc]));
          workingArray.push(tempObj);
        }
        //Sort locations by that number
        workingArray.sort(function(a,b){
          if (a.seatsTaken == b.seatsTaken){return 0;}
          if(a.seatsTaken > b.seatsTaken){
            return -1;
          } else {
            return 1;
          }
        });
        var output = [];
        for (var loc in workingArray){
          output.push(workingArray[loc].name);
        }
        return output;
    }
  }]).
filter('sortUsersByLocationOnDay', function(){
  return function(input, dayObj, reverse){
    if(dayObj == ''){return input;}
    var objArray=[], data, returnArray=[];
    for (var i=0; i<input.length; i++){
      data={};
      data.user = input[i];
      if(dayObj.people.hasOwnProperty(input[i])){
        data.location = dayObj.people[input[i]].location;
      } else {
        data.location = '';
      }
        objArray.push(data);
    }
    objArray.sort(function(a,b){
      if(a.location < b.location){
        return -1;
      } else if(a.location > b.location){
        return 1;
      } else {return 0}
    });
    for (var i=0; i<objArray.length; i++){
      returnArray.push(objArray[i].user);
    }
    if(reverse){return returnArray.reverse();}
    return returnArray;

  }
}).
filter('sortArrayByMappedProperty', ['$filter', function(filter){
  return function(input, map, propertyToSortBy, reverse){
    var sortableArray=[],
        tempObj={};
    for(var i=0; i<input.length; i++){
      tempObj={};
      tempObj.id= input[i];
      tempObj[propertyToSortBy]=map[input[i]][propertyToSortBy];
      sortableArray.push(tempObj);
    }
    var returnArray = filter('orderBy')(sortableArray, propertyToSortBy, reverse);
    return returnArray.map(function(element, index){
      return element.id;
    });
  }
}]).
filter('sortWeekView', ['$filter', function(filter){
  return function(input, predicate, reverse){
    if(predicate.hasOwnProperty("Year")){
      return (filter('sortUsersByLocationOnDay')(input,predicate,reverse));
    }
    else{
      return filter('sortArrayByMappedProperty')(input, predicate, "firstName", reverse);
    }
    return input;
}
}]);
