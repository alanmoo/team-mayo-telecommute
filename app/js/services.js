'use strict';

/* Services */


angular.module('telecommuteApp.services', []).
  constant('MS_PER_DAY', 86400000).
  constant('DAYS_IN_MONTH', [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]).
  constant('MONTH', ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]).
  service('getDaysIn', function(DAYS_IN_MONTH){
    this.month = function(year, month){
        if(month !== 1){
            return DAYS_IN_MONTH[month];
        }
        else{
            if (year%4 === 0){
                return 29;
            }
            else{
                return 28;
            }
        }
    };
  }).
    factory('calendarGenerator', function(MS_PER_DAY, getDaysIn, MONTH){
        return{
            isToday: function(dayObj){
                var monthIndex = MONTH.indexOf(dayObj.Month);
                var dayObjDate = new Date(dayObj.Year, monthIndex , dayObj.day);
                if(dayObjDate.toDateString() === new Date().toDateString()){
                    return "today";
                }
                else {
                    return "";
                }
            },
            queryFormatDate: function(date){
                function leadZero(num){
                    if(num < 10){return '0'+num;} else{return num;}
                }
                return leadZero(date.getDate()+'-'+MONTH[date.getMonth()]+'-'+date.getFullYear());
            },
        getFirstSunday: function(month, year){
            var firstDay = new Date(year, month, 1).getDay(),
                firstSunday = new Date(year, month, 1-firstDay);
                return firstSunday;
            },
            getLastSaturday: function(date, firstSunday){
                // //While last month still has days left, add those days to array
                var day1 = firstSunday.getDate(),
                    year = date.getFullYear(),
                    month = date.getMonth(),
                    day_count = 0;
                
                //If the first Sunday is in the previous month, get amount of remaining days in that month
                if(day1 !== 1){
                    for (var x = day1; x <= getDaysIn.month(year, firstSunday.getMonth()) && x > 1; x++){
                        day_count++;
                    }
                }
                // //Add all days of this month to array
               day_count += getDaysIn.month(year, date.getMonth());

                // //If this month ends before Saturday, figure out how many days are left to fill up the week
                if(day_count%7===0){
                    return new Date(year, month, getDaysIn.month(year, firstSunday.getMonth()));
                } else{
                    return new Date(this.getYearOfNextMonth(month,year), this.getNextMonth(month), 7-day_count%7);
               }
            },
            getNextMonth: function(month){
                if(month >= 11){
                    return 0;
                }else{
                    month++;
                    return month;
                }
            },
            getPrevMonth: function(month){
                if(month <= 0){
                    return 11;
                }else{
                    month--;
                    return month;
                }   
            },
            getYearOfNextMonth: function(month, year){
                //Safety measure
                if(month >= 11){
                    year++
                    return year;
                }
                else{
                    return year;
                }
            },
            getYearOfPrevMonth: function(month, year){
                if (month <= 0){
                    year--;
                    return year;
                }
                else{
                    return year;
                }
            },
            getWeek: function(year, month, weekNum){
                var week = {};
                var dateInWeek = new Date(year, month, (weekNum-1)*7+1),
                    dayInWeek = dateInWeek.getDay(),
                    firstOfWeek = dateInWeek-(MS_PER_DAY*dayInWeek);

                week.firstDateOfWeek = new Date(firstOfWeek);
                week.lastDateOfWeek = new Date(year, week.firstDateOfWeek.getMonth(), week.firstDateOfWeek.getDate()+6);
                return week;
            },
            shortName: function(month){
                var shortname = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
                return shortname[month];
            }
        };
    }).
    factory('dataManipulation', ['$http','$rootScope', function($http, $rootScope){
        return{
            setLocation: function(userID, location, dayObj){
                function paramaterizeData(){
                    var dataString = userID+','+$rootScope.statusID[location]+','+dayObj.day+'-'+dayObj.Month+'-'+dayObj.Year;
                    return dataString;
                }
                if(!dayObj.people.hasOwnProperty(userID)){
                    dayObj.people[userID]={};
                }
                dayObj.people[userID].location = "Saving...";
                $http.get('http://140.251.8.194/teamMayo/updateCalData.cfm?data='+paramaterizeData()).success(function(){
                    dayObj.people[userID].location = location;                    
                });
            },
            doesUserManage: function(user, nameMap, cwid){
                if(nameMap[user].userrole === 'admin'){
                    return true;
                }
                if(!(nameMap[user].userrole === 'manager')){
                    return false;
                }
                if(user === cwid){
                    return false;
                }
                if(nameMap[user].departmentid === nameMap[cwid].departmentid){
                    return true;
                }
                else {return false;}
            }
        };
    }]).
    factory('fetchData', ['$http', function($http){
        var userList = null,
            statusList = null,
            groupsList = null,
            statusID = null;

        var statusIDPromise = $http.get('http://140.251.8.194/teamMayo/getStatusId.cfm')
                          .success(function(data){
                            statusID = data;
                          })
                          .error(function(){
                                window.console.log("Unable to fetch Status ID list");
                          });
        var groupsListPromise = $http.get('http://140.251.8.194/teamMayo/getGroupsUsersList.cfm')
                                .success(function(data){
                                    groupsList = data;
                                })
                                .error(function(){
                                    window.console.log("Unable to fetch group list");
                                });
        var userListPromise = $http.get('http://140.251.8.194/teamMayo/getUsersData.cfm')
                           .success(function(data){
                                userList = data;
                            })
                           .error(function(){
                                window.console.log("Unable to fetch user list");
                           });
        var statusListPromise = $http.get('http://140.251.8.194/teamMayo/getStatusList.cfm')
                                .success(function(data){
                                    statusList = data;
                                })
                                .error(function(){
                                    window.console.log("Unable to fetch status list");
                                });
        var getStatusID = function(){
            return statusID;
        }         
        var getUserList = function(){
            return userList;
        };

        var getStatusList = function(){
            return statusList;
        };

        var getGroupsList = function(){
            return groupsList;
        };

        // var getAllUsers = function(){
        //     var deferred = $q.defer();
        //     getUserList().then(function(data){
        //         deferred.resolve(Object.keys(data));
        //     });
        //     return deferred.promise;
        // };

        return {userListPromise: userListPromise,
                statusListPromise: statusListPromise,
                groupsListPromise: groupsListPromise,
                statusIDPromise: statusIDPromise,
                getUserList: getUserList,
                getStatusList: getStatusList,
                getGroupsList: getGroupsList,
                getStatusID: getStatusID};
    }]);
