<?php
	header('content-type: application/json; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET');  
	function convertToDateObj($date){
		$dateObj = preg_split("/-/", $date);
		$months = ["jan", "feb", "mar", "april", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"];
		$dateObj[1] = array_search($dateObj[1], $months)+1;
		$returnDate = date_create($dateObj[0]."-".$dateObj[1]."-".$dateObj[2]);
		return $returnDate;
	}
	function getWeekOfData($date){
		$endDate = clone $date;
		$endDate->add(new DateInterval('P6D'));
		getRangeofData($date, $endDate);
	}

	function getRangeofData($startDate, $endDate){
		try {
		  $conn = new PDO('mysql:host=localhost;dbname=telecommute', 'root', 'root');
		  $stmt = $conn->prepare('SELECT a.cwid, a.location, a.day, b.statusid, b.statusname
	        FROM userLocationMapping a, statuses b
	        WHERE a.location = b.statusid');
		  $stmt->execute();
		  $endDate->modify( '+1 day');
		  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		  $interval = new DateInterval('P1D');
		  // echo "start:<br>";
		  // var_dump($startDate);
		  // echo "<br>period:<br>";
		  // var_dump($interval);
  		//   echo "<br>end:<br>";
  		//   var_dump($endDate);
		  $output =[];
		  $period = new DatePeriod($startDate, $interval, $endDate);
		  foreach( $period as $dt){
		  	$longDate = $dt->format('Ymd');
			$output[$longDate]["day"] = $dt -> format('d');
			$output[$longDate]["year"] = $dt -> format('Y');
			$output[$longDate]["dayOfWeek"] = $dt -> format('l');
			$output[$longDate]["month"] = $dt -> format('F');
			foreach($result as $key=>$value){
				if($value["day"] == $dt->format('l')){
				$output[$longDate]["people"][$value["cwid"]]["location"]=$value["statusname"];
				} else if ($dt->format('l') == "Saturday" || $dt->format('l') == "Sunday") {
					$output[$longDate]["people"]=[];
				}
			}
		  }


		  
		  // $output = [];
		  // foreach ($result as $index => $person) {
			 //  	$output[$person[cwid]] = [];
		  // 	foreach ($person as $key => $value) {
		  // 		if ($key != "cwid") {
		  // 			$output[$person[cwid]][$key]= $value;
		  // 		}
		  // 	}
		  // }
	      echo json_encode($output); 
		  if ( count($result) ) { 
		    foreach($result as $row) {
		    }   
		  } else {
		    echo "No rows returned.";
		  }
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}	
	}

	if(isset($_GET['fromDate'])){
		if(isset($_GET['toDate'])){
			die(getRangeofData(convertToDateObj($_GET['fromDate']), convertToDateObj($_GET['toDate'])));
		} else {
		die(getWeekOfData(convertToDateObj($_GET['fromDate'])));
		}
	}
	
?>
