<?php
	header('content-type: application/json; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET');  
	try {
	  $conn = new PDO('mysql:host=localhost;dbname=telecommute', 'root', 'root');
	  $stmt = $conn->prepare('SELECT c.groupName, c.groupid, d.cwid
	        FROM groups c, users d
	        WHERE d.departmentid = c.groupid');
	  $stmt->execute();
	 
	  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  $output = [];
	  foreach ($result as $index => $data) {
	  	// echo $data['groupName'];
	  	// echo $data['cwid'];
	  	if(!isset($output[$data['groupName']])){
	  		$output[$data['groupName']] = [];
	  	}
		array_push($output[$data['groupName']], $data['cwid']);
	  }
	  // var_dump($output);
      echo json_encode($output); 
	  if ( count($result) ) { 
	    foreach($result as $row) {
	    }   
	  } else {
	    echo "No rows returned.";
	  }
	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}
?>