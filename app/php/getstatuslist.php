<?php
	header('content-type: application/json; charset=utf-8');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET');  
	try {
	  $conn = new PDO('mysql:host=localhost;dbname=telecommute', 'root', 'root');
	  $stmt = $conn->prepare('SELECT * FROM statuses');
	  $stmt->execute();
	  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	  $output = [];

	  foreach ($result as $key => $value) {
	  	$output[$value["statusname"]]["capacity"] = $value["capacity"];
	  }
	  echo json_encode($output);
	  if ( count($result) ) { 
	    foreach($result as $row) {
	    }   
	  } else {
	    echo "No rows returned.";
	  }
	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}
?>