'use strict';

/* jasmine specs for services go here */

describe('service', function() {
  beforeEach(module('telecommuteApp.services'));

    describe('getLastSaturday', function(){

        it('should output August 3 for July 2013', inject(function(calendarGenerator){
            var today= new Date(2013, 6, 15);
            var firstSunday = new Date(2013, 5, 30);
            expect(calendarGenerator.getLastSaturday(today, firstSunday)).toEqual(new Date(2013, 7, 3));
        }));

        it('should output August 31 for August 2013', inject(function(calendarGenerator){
            var today= new Date(2013, 7, 15);
            var firstSunday = new Date(2013, 6, 28);
            expect(calendarGenerator.getLastSaturday(today, firstSunday)).toEqual(new Date(2013, 7, 31));
        }));

        it('should output October 5 for September 2013', inject(function(calendarGenerator){
            var today= new Date(2013, 8, 15);
            var firstSunday = new Date(2013, 8, 1);
            expect(calendarGenerator.getLastSaturday(today, firstSunday)).toEqual(new Date(2013, 9, 5));
        }));
        it('should output March 5 for February 2016', inject(function(calendarGenerator){
            var today= new Date(2016, 1, 15);
            var firstSunday = new Date(2016, 0, 31);
            expect(calendarGenerator.getLastSaturday(today, firstSunday)).toEqual(new Date(2016, 2, 5));
        }));
    });
    describe('getFirstSunday', function(){
        it('should output July 28 for August 2013', inject(function(calendarGenerator){
            var month = 7;
            var year = 2013;
            var expectation = new Date(2013, 6, 28);
            expect(calendarGenerator.getFirstSunday(month, year)).toEqual(expectation);
        }));
        it('should output September 1 for September 2013', inject(function(calendarGenerator){
            var month = 8;
            var year = 2013;
            var expectation = new Date(2013, 8, 1);
            expect(calendarGenerator.getFirstSunday(month, year)).toEqual(expectation);
        }));
    });
    describe('getWeek', function(){
        it('should return July 28 - August 3 for Week 1 August 2013', inject(function(calendarGenerator){
                var week = calendarGenerator.getWeek(2013, 7, 1);
                expect(week.firstDateOfWeek).toEqual(new Date(2013,6,28));
                expect(week.lastDateOfWeek).toEqual(new Date(2013, 7, 3));
        }));
        it('should return Feb 28 - March 4 for Week 5 February 2016', inject(function(calendarGenerator){
                var week = calendarGenerator.getWeek(2016, 1, 5);
                expect(week.firstDateOfWeek).toEqual(new Date(2016,1,28));
                expect(week.lastDateOfWeek).toEqual(new Date(2016, 2, 5));
        }));
    });
});
